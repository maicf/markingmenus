/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package markingmenus;

import javax.swing.JLayer;
import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;

/**
 *
 * @author Mai
 */
public class Command extends Item {
    

    
    public Command(String name) {
        super(name);
    }
    
        
    public Item select() {
        
        System.out.println("Selected command: "+name);
        return null;
    }
    
    //public abstract Object execute(Object param);
    
    public void execute(JLayer l) {
        JTextPane textEditor = (JTextPane)l.getView();
        StyledDocument doc = textEditor.getStyledDocument();
        doc.setCharacterAttributes(textEditor.getSelectionStart(), 
                                   textEditor.getSelectionEnd()- textEditor.getSelectionStart() , 
                                   textEditor.getStyle(name), true);
   
    }

    @Override
    public void restartSelection() {
        
    }
}
