package markingmenus;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayer;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.border.LineBorder;
import javax.swing.plaf.LayerUI;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

/**
 *
 * @author Mai
 */
public class TextEditor{
      private static final Font EDITOR_FONT =new Font("Lato", Font.PLAIN, 12);
      private static final Font LINEAR_MENU_FONT =new Font("Lato", Font.PLAIN, 16);
      private static final Font METRICS_FONT =new Font("Lato", Font.PLAIN, 16);
      private static final Color defaultColor = new Color(133,194,255);
      private static final Color metricsColor = new Color(250,46,89);
      private static StyleContext sc;
      private static JMenuBar mainMenu;
      private static JMenu styleMenu, fontMenu, alignMenu, sizeMenu;
      private static long timeLinear;
      private static final JLabel metrics = new JLabel("",JLabel.CENTER);
      private static boolean linearDemo = false; 
              
      public static void main(String[] args) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    createUI();
                }
            });
      }
      
      
      
      public static void createUI() {
            JFrame f = new JFrame("A simple text editor");
            f.setSize(800, 600);
            f.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
            f.setLocationRelativeTo (null);
            f.setResizable(false);
            
            KeyAdapter keyAdapter = new KeyAdapter() { 
                public void keyPressed(KeyEvent e) {
                    if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
                        System.out.println("Escape detected");
                        linearDemo=!linearDemo;
                        if (linearDemo) {
                            timeLinear = System.currentTimeMillis();
                            metrics.setText("Time: 0.0 s");
                            metrics.setVisible(true);
                        }
                        else 
                            metrics.setVisible(false);

                    }
                    

                }
            };
                    
            JPanel p = new JPanel();
            p.setSize(800,600);
            
            sc = new StyleContext();
            final DefaultStyledDocument doc = new DefaultStyledDocument(sc);
            JTextPane textEditor = new JTextPane(doc);
            final Style mainStyle = sc.addStyle("mainStyle", null);
            StyleConstants.setFontSize(mainStyle, 12);
            doc.setLogicalStyle(0, mainStyle);
            textEditor.setPreferredSize(new Dimension(790,500));
            textEditor.setBorder(new LineBorder(defaultColor, 1, false));
            textEditor.setFont(EDITOR_FONT);              
            metrics.setVisible(false);
            metrics.setVerticalAlignment(JLabel.BOTTOM);
            metrics.setHorizontalAlignment(JLabel.CENTER);
            metrics.setFont(METRICS_FONT);
            metrics.setForeground(metricsColor);
            
            
            LayerUI<JComponent> layerUI = new MarkingLayerUI(createMenuStructure(),metrics);
            JLayer<JComponent> jlayer = new JLayer<>(textEditor, layerUI);
            
            mainMenu = new JMenuBar();
            mainMenu.setBorder(new LineBorder(defaultColor, 1, false));
            
            styleMenu = new JMenu("Style");
            styleMenu.setFont(LINEAR_MENU_FONT);
           
            JMenuItem bold = new JMenuItem("Bold");
            bold.setFont(LINEAR_MENU_FONT);
            styleMenu.add(bold);
           
            JMenuItem italics = new JMenuItem("Italics");
            italics.setFont(LINEAR_MENU_FONT);
            styleMenu.add(italics);

            JMenuItem strikethrough = new JMenuItem("Strikethrough");
            strikethrough.setFont(LINEAR_MENU_FONT);
            styleMenu.add(strikethrough);
            
            JMenuItem underline = new JMenuItem("Underline");
            underline.setFont(LINEAR_MENU_FONT);
            styleMenu.add(underline);
            
            fontMenu = new JMenu("Font");
            fontMenu.setFont(LINEAR_MENU_FONT);
            
            JMenuItem arial = new JMenuItem("Arial");
            arial.setFont(LINEAR_MENU_FONT);
            fontMenu.add(arial);
            JMenuItem serif = new JMenuItem("Serif");
            serif.setFont(LINEAR_MENU_FONT);
            fontMenu.add(serif);
            JMenuItem sanserif = new JMenuItem("Sans Serif");
            sanserif.setFont(LINEAR_MENU_FONT);
            fontMenu.add(sanserif);
            JMenuItem times = new JMenuItem("Times New Roman");
            times.setFont(LINEAR_MENU_FONT);
            fontMenu.add(times);
            
            alignMenu = new JMenu("Alignment");
            alignMenu.setFont(LINEAR_MENU_FONT);
            
            JMenuItem center = new JMenuItem("Center");
            center.setFont(LINEAR_MENU_FONT);
            alignMenu.add(center);
            JMenuItem justified = new JMenuItem("Justified");
            justified.setFont(LINEAR_MENU_FONT);
            alignMenu.add(justified);
            JMenuItem left = new JMenuItem("Left");
            left.setFont(LINEAR_MENU_FONT);
            alignMenu.add(left);
            JMenuItem right = new JMenuItem("Right");
            right.setFont(LINEAR_MENU_FONT);
            alignMenu.add(right);
            
            
            sizeMenu = new JMenu("Size");
            sizeMenu.setFont(LINEAR_MENU_FONT);
            
            JMenuItem extralarge = new JMenuItem("Extra Large");
            extralarge.setFont(LINEAR_MENU_FONT);
            sizeMenu.add(extralarge);
            JMenuItem medium = new JMenuItem("Medium");
            medium.setFont(LINEAR_MENU_FONT);
            sizeMenu.add(medium);
            JMenuItem large = new JMenuItem("Large");
            large.setFont(LINEAR_MENU_FONT);
            sizeMenu.add(large);
            JMenuItem small = new JMenuItem("Small");
            small.setFont(LINEAR_MENU_FONT);
            sizeMenu.add(small);
            
            mainMenu.add(styleMenu);
            mainMenu.add(fontMenu);
            mainMenu.add(alignMenu);
            mainMenu.add(sizeMenu);
            
                        
            p.add(mainMenu);
            p.add(jlayer);
            p.add(metrics);
                    
            
            f.add(p);

            
            
            ActionListener itemListener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (linearDemo) {
                        timeLinear = System.currentTimeMillis() - timeLinear;
                        metrics.setText("Time: "+timeLinear/(float)1000+" s");
                        metrics.setVisible(true);
                        timeLinear = System.currentTimeMillis();
                    }

                }


            };
            
            bold.addActionListener(itemListener);
            italics.addActionListener(itemListener);
            strikethrough.addActionListener(itemListener);
            underline.addActionListener(itemListener);
            left.addActionListener(itemListener);
            right.addActionListener(itemListener);
            center.addActionListener(itemListener);
            justified.addActionListener(itemListener);
            small.addActionListener(itemListener);
            medium.addActionListener(itemListener);
            large.addActionListener(itemListener);
            extralarge.addActionListener(itemListener);
            serif.addActionListener(itemListener);
            sanserif.addActionListener(itemListener);
            times.addActionListener(itemListener);
            arial.addActionListener(itemListener);

            jlayer.setFocusable(true);
            jlayer.addKeyListener(keyAdapter);
            textEditor.addKeyListener(keyAdapter);
            
            f.setVisible (true);
       }
      
       private static Menu createMenuStructure() {
        
        Menu markingMenu = new Menu("Text");
        String commandName;
              
        Menu style = new Menu("Style");
        

        commandName = "Bold";
        style.addItem(new Command(commandName));
        final Style bold = sc.addStyle(commandName, null);
        bold.addAttribute(StyleConstants.Bold, true);
        
        commandName = "Italics";    
        style.addItem(new Command(commandName));
        final Style italics = sc.addStyle(commandName, null);
        italics.addAttribute(StyleConstants.Italic, true);
        
        commandName = "Underline";    
        style.addItem(new Command(commandName));
        final Style underline = sc.addStyle(commandName, null);
        underline.addAttribute(StyleConstants.Underline, true);
      
        commandName = "Strikethrough";    
        style.addItem(new Command(commandName));
        final Style strikethrough = sc.addStyle(commandName, null);
        strikethrough.addAttribute(StyleConstants.StrikeThrough, true);
        
        markingMenu.addItem(style);
               
        Menu size = new Menu("Size");
        
        commandName = "Small";  
        size.addItem(new Command(commandName));
        final Style small = sc.addStyle(commandName, null);
        StyleConstants.setFontSize(small, 10);
        
        commandName = "Medium";  
        size.addItem(new Command(commandName));
        final Style medium = sc.addStyle(commandName, null);
        StyleConstants.setFontSize(medium, 12);
        
        commandName = "Large";  
        size.addItem(new Command(commandName));
        final Style large = sc.addStyle(commandName, null);
        StyleConstants.setFontSize(large, 16);
        
        commandName = "Extra Large";  
        size.addItem(new Command(commandName));
        final Style extralarge = sc.addStyle(commandName, null);
        StyleConstants.setFontSize(extralarge, 20);
        
        markingMenu.addItem(size);
        
        Menu alignment = new Menu("Alignment");
        
        commandName = "Center";  
        alignment.addItem(new Command(commandName));
        final Style center = sc.addStyle(commandName, null);
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        
        commandName = "Left";  
        alignment.addItem(new Command(commandName));
        final Style left = sc.addStyle(commandName, null);
        StyleConstants.setAlignment(left, StyleConstants.ALIGN_LEFT);
        
        commandName = "Justified";  
        alignment.addItem(new Command(commandName));
        final Style justified = sc.addStyle(commandName, null);
        StyleConstants.setAlignment(justified, StyleConstants.ALIGN_JUSTIFIED);
        
        commandName = "Right";  
        alignment.addItem(new Command(commandName));
        final Style right = sc.addStyle(commandName, null);
        StyleConstants.setAlignment(right, StyleConstants.ALIGN_RIGHT);
        
        markingMenu.addItem(alignment);
        
        Menu font = new Menu("Font");
        
        commandName = "Sans Serif";  
        font.addItem(new Command(commandName));
        final Style sanserif = sc.addStyle(commandName, null);
        StyleConstants.setFontFamily(sanserif, "sansserif");
        
        commandName = "Times";  
        font.addItem(new Command(commandName));
        final Style times = sc.addStyle(commandName, null);
        StyleConstants.setFontFamily(times, "Times New Roman");
        
        commandName = "Serif";  
        font.addItem(new Command(commandName));
        final Style serif = sc.addStyle(commandName, null);
        StyleConstants.setFontFamily(serif, "serif");
       
        commandName = "Arial";  
        font.addItem(new Command(commandName));
        final Style arial = sc.addStyle(commandName, null);
        StyleConstants.setFontFamily(arial, "Arial");
        
        
        markingMenu.addItem(font);
        
        return markingMenu;
     }

            
    
}
