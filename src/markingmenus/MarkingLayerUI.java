package markingmenus;

import java.awt.AWTEvent;
import java.awt.AWTException;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JLayer;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.plaf.LayerUI;

/**
 *
 * @author Mai
 */
public class MarkingLayerUI extends LayerUI<JComponent>  {
                        
            private final int NONE = -1;
            private final Menu markingMenu;
            private StrokeRecogniser recogniser;
            
            private Menu currentMenu;
            private int slice = NONE;
            private boolean cancel;
            private boolean buttonPressed;
            private Point previous;
            private Point lastCenter;
            private Point previousCenter;
            
            private long time;
            private long selectionTime;
            private Timer markingTimer;
            private final int markingDelay = 300;
            private final int toastDelay = 400;
            private final int depth = 2;
            private final int radius = 50;
            private final int innerRadius = 20;
            private final int itemTagSize = radius - innerRadius;
            private final int offsetMarginX = 25;
            private final int offsetMarginY = 10;
            private final int edgeMarginX = radius + offsetMarginX;
            private final int edgeMarginY = radius + offsetMarginY;
            private final boolean mouseWarping = true; // change it to false to test in Ubuntu!
            private final int activationButton = 3; //right click
            private final boolean noCatSelection; //only if depth = 2, otherwise marks are scale dependent or menu rotation needed
            private final JLabel metrics;
                    
            private Graphics2D graphics;
            private final float thickness = 3;
            private final Color defaultColor = new Color(77,109,178);
            private final Color applied = new Color(86,204,87);
            private final Color mark = new Color(250,46,89);
            private  final Font MARKING_FONT =new Font("Lato", Font.PLAIN, 16);
            private  final Font APPLIED_FONT =new Font("Lato", Font.PLAIN, 16);
            private final HashMap newHints;
        
            private boolean showStrokes = false; // set to true for debugging the recognition algorithm
            
            public MarkingLayerUI(Menu markingMenu, JLabel metrics) {
                this.markingMenu = markingMenu;
                this.metrics = metrics;
                previous = new Point();
                newHints = new HashMap();
                newHints.put(RenderingHints.KEY_ANTIALIASING,
                             RenderingHints.VALUE_ANTIALIAS_ON);
                newHints.put(RenderingHints.KEY_COLOR_RENDERING,
                             RenderingHints.VALUE_COLOR_RENDER_QUALITY);
                newHints.put(RenderingHints.KEY_RENDERING,
                             RenderingHints.VALUE_RENDER_QUALITY);
                newHints.put(RenderingHints.KEY_TEXT_ANTIALIASING,
                             RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                noCatSelection = (depth==2);
            }
            
            

            @Override
            public void installUI(JComponent c) {
                super.installUI(c);
                JLayer jlayer = (JLayer)c;
                jlayer.setLayerEventMask(                    //registers for mouse events
                  AWTEvent.MOUSE_EVENT_MASK |
                  AWTEvent.MOUSE_MOTION_EVENT_MASK
                );
            }

            @Override
            public void uninstallUI(JComponent c) {
              JLayer jlayer = (JLayer)c;
              jlayer.setLayerEventMask(0);
              super.uninstallUI(c);
            }

        
                @Override
                public void paint(Graphics g, JComponent c) {
                    super.paint(g, c);  // repaints the componentOnScreen under the transparent layer (i.e. jtextarea for us)

                }

                @Override
                protected void processMouseEvent(MouseEvent e, JLayer l) {
                  buttonPressed=false;
                  if (e.getButton()==activationButton) {
                      if (e.getID() == MouseEvent.MOUSE_PRESSED) {
                          buttonPressed=true;
                          processMousePressed(e.getX(), e.getY(), l);
                      }
                      else if (e.getID() == MouseEvent.MOUSE_RELEASED)
                          processMouseReleased(e.getX(), e.getY(),l);
                  }
                  
                }

                @Override
                protected void processMouseMotionEvent(MouseEvent e, JLayer l) {
                      if (buttonPressed && (e.getID() == MouseEvent.MOUSE_DRAGGED))
                          processMouseDragged(e.getX(), e.getY(),l);
                  
                }
                

        


    
    private void renderString(String name, int x, int y) { 
            //to do: show the text with some shadow or some nice visual effect
            graphics.drawString(name, x, y);

    }
    
    private Point polarToCartesian(int x, int y, double angle, double radius) {
      Point pt = new Point();
      pt.x = (int)(x + radius*Math.cos(angle));
      pt.y = (int)(y - radius*Math.sin(angle));
//    uncomment next code if necessary:
//      if (Math.abs(componentOnScreen.x - radius) <= 1) {
//         componentOnScreen.x = (int) radius;
//      }
//      if (Math.abs(componentOnScreen.y - radius) <= 1) {
//         componentOnScreen.y = (int) radius;
//      }

      return pt;
   }
    

    
    private void showMenuItem(String name, double angle, int centerX, int centerY) {
        Point pointText = polarToCartesian(centerX, centerY, angle, radius);
        pointText = centerMenuItem(pointText, name);
        renderString(name, pointText.x, pointText.y);
    }
  
    
    private void showMenu(int x, int y, JLayer l) { //render the whole menu, with the center in (x,y)
         currentMenu.setDisplayed(true); 
         System.out.println("Showing menu "+currentMenu.getName());
         Point newCenter = new Point(x,y);
         if (mouseWarping)
            newCenter = overlapWarpPoint(x,y,l);
         showMenuCircle(newCenter.x,newCenter.y);
         previousCenter = (Point)lastCenter.clone();
         lastCenter = new Point();
         lastCenter.x = newCenter.x;
         lastCenter.y = newCenter.y;
         double amplitude = 2*Math.PI / currentMenu.getItemsCount();
         double currentRadian = amplitude/2;
         for (int i =0; i<currentMenu.getItemsCount(); i++) {
             Item item = currentMenu.getItem(i);
             if (item!=null) {
                String itemName = item.getName();
                drawMenuItemTab(newCenter.x, newCenter.y, currentRadian + 1 * amplitude);
                showMenuItem(itemName, currentRadian + 0.5*amplitude,newCenter.x,newCenter.y);
             }
             else
                 System.out.println("currentMenu: "+ currentMenu.getName() +", item "+ i +" is null");
             currentRadian+= amplitude;
         }
         
         

    } 
    
     private Point overlapWarpPoint(int x, int y, JLayer l) { //when marking near the previous menu, do mouse warping
        
            Point original = new Point(x,y); //keep a reference of original mouse coordinates
        
            int dX = previousCenter.x - x;
            int dY = previousCenter.y - y;
            int distX = Math.abs(dX);
            int distY = Math.abs(dY);

            if ( !(distX==0 && distY==0) && distX< 2*radius && distY< 2*radius ) { //overlapping
                System.out.println("Overlapping detected");
                if (distX >= distY) {
                    if (dX<0) { //new menu on the right of previous one
                        x = x + 2* radius + offsetMarginX; //move new menu to the right to avoid overlapping
                        System.out.println("Moving menu to the right");
                    }
                    else if (dX>0) { //new menu on the left of previous one
                        x = x - 2*radius - offsetMarginX; //move new menu to the left to avoid overlapping
                        System.out.println("Moving menu to the left");
                    }
                }
                else {
                    if (dY < 0) {// new menu below previous one
                        y = y + 2 * radius; //move new menu below to avoid overlapping
                        System.out.println("Moving menu below");
                    }
                    else if (dY > 0 ) {// new menu above previous one
                        y = y - 2 * radius; // move new menu above to avoid overlapping
                        System.out.println("Moving menu above");
                    }
                }
                Point toOverlapWarp = new Point(x,y);  // new mouse coordinates in parent's coordinates system
                if (!nearEdge(toOverlapWarp.x,toOverlapWarp.y,l)) { //if new position near edge, we don't apply overlapping warping
                    warpPoint(original,toOverlapWarp,l);
                    graphics.drawLine(original.x,original.y, toOverlapWarp.x, toOverlapWarp.y);
                    previous = (Point)toOverlapWarp.clone();
                    return toOverlapWarp; //return point to continue working in parent coordinates system
                }
                return original;
                //else //force edgewarping over overlappwarping - awkward behaviour!
                //    return edgeWarpPoint(toOverlapWarp.x, toOverlapWarp.y, l);
                    
            }
            
            return original;
      
       
    }
     
     private boolean nearEdge(int x, int y, JLayer l) {
         return x < edgeMarginX || 
                x > l.getView().getWidth() - edgeMarginX ||
                y < edgeMarginY ||
                y > l.getView().getHeight() - edgeMarginY;
     }
     
     
     private Point warpPoint(Point original, Point toWarp, JLayer l) {
         
        Point componentOnScreen = new Point(l.getView().getLocation()); //parent location on app coordinates system
        SwingUtilities.convertPointToScreen(componentOnScreen, l.getView()); //parent location on screen coordinates system
        Point warped = new Point(componentOnScreen.x + toWarp.x, componentOnScreen.y + toWarp.y); //new point on screen coord. sys.
        if (!toWarp.equals(original)) {
            Robot mouseRobot;
                    try {
                        mouseRobot = new Robot();
                        mouseRobot.mouseMove(warped.x,warped.y); //move mouse to new (x,y), in screen coord. sys
                    } catch (AWTException ex) {
                        Logger.getLogger(MarkingLayerUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
            return toWarp; //return point to continue working in parent coordinates system
        }
        return original; //warp not performed
     }
     
    private Point edgeWarpPoint(int x, int y, JLayer l) { //when marking near the edge, do mouse warping
        Point original = new Point(x,y); //keep a reference of original mouse coordinates
        if (x < edgeMarginX)
            x = edgeMarginX;
        else if (x > l.getView().getWidth() - edgeMarginX )
            x = l.getView().getWidth() - edgeMarginX;
        if (y < edgeMarginY)
            y = edgeMarginY;
        else if (y > l.getView().getHeight() - edgeMarginY )
            y = l.getView().getHeight() - edgeMarginY;
        
        Point toWarp = new Point(x,y);  // new mouse coordinates in parent's coordinates system
        return warpPoint(original,toWarp,l);
          
       
    }
    
        private void processMousePressed(int x, int y, final JLayer l) {
            time= System.currentTimeMillis();
            selectionTime = System.currentTimeMillis();
            metrics.setVisible(false);
            markingMenu.restartSelection();
            slice = NONE;
            currentMenu = markingMenu;
            cancel = false;
            Point p = new Point(x,y);
            if (mouseWarping)
              p = edgeWarpPoint(x,y,l);
            previous.x = p.x;
            previous.y = p.y ;
            lastCenter = (Point)previous.clone();
            previousCenter = (Point)lastCenter.clone();//try with null if it doesn't work
            ActionListener actionListener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    markingTimer.stop();  //removing this line causes erratic behaviour!
                    showMenu(previous.x,previous.y,l);
                }
            };
            if (!currentMenu.isShowing()) {
                markingTimer = new Timer(markingDelay, actionListener);
                markingTimer.setRepeats(false);
                markingTimer.start();
            }
            l.getView().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            graphics = (Graphics2D) l.getGraphics().create();
            graphics.setStroke(new BasicStroke(thickness));
            graphics.setColor(defaultColor);
            graphics.setFont(MARKING_FONT);
            graphics.setRenderingHints(newHints);
            
            recogniser = new StrokeRecogniser( currentMenu.getItemsCount(),innerRadius,depth);   //recognises a mark (stroke)
    } 
    
    private void updateMetrics() {
        float selectionSeconds = selectionTime/(float)1000;
        metrics.setText("Time: "+selectionSeconds+" s");
        metrics.setVisible(true);
    }

    
    public void processMouseReleased(int x, int y, final JLayer l) { 
            markingTimer.stop();
            currentMenu.setDisplayed(false);    
            l.getView().setCursor(new Cursor(Cursor.TEXT_CURSOR));
            if (!cancel && recogniser.outOfInactiveArea(previous, lastCenter)) {
                Item selectedItem = currentMenu.getSelectedItem();
                if (slice!=NONE && selectedItem==null && noCatSelection) { // enforcing "no category selection". ONLY WORKS WITH DEPTH 2, OTHERWISE IT'S AMBIGUOUS
                    currentMenu.selectItem(slice);
                    selectedItem = currentMenu.getSelectedItem();
                }
                selectionTime = System.currentTimeMillis() - selectionTime;
                updateMetrics();
                if (selectedItem!=null) {
                    selectedItem.execute(l);
                    graphics.setColor(applied);
                    graphics.setFont(APPLIED_FONT);
                    renderString(selectedItem.getName()+ " applied!",x+20,y+30); //Show applied command
                }
               
                ActionListener toastListener = new ActionListener() {
                public void actionPerformed(ActionEvent actionEvent) {
                      l.repaint();
                }
                };
                Timer toastTimer = new Timer(toastDelay,toastListener);
                toastTimer.setRepeats(false);
                toastTimer.start();
            }
            else 
              l.repaint();                                         //repaints the JLayer so the last marks disappear
            
    }
   
  
    public void processMouseDragged(int x,int y, JLayer l) {
                    markingTimer.stop(); //removing this line causes erratic behaviour!
                    if (!cancel) {    
                        Point newPoint = new Point();
                        newPoint.x = x;
                        newPoint.y = y;
                        if (recogniser.isRecognising()) {
                            time= System.currentTimeMillis()-time;
                            if (!currentMenu.isShowing() ||
                               recogniser.outOfInactiveArea(newPoint, lastCenter)) // to avoid repainting menu if user didn't go outside
                               markingTimer.start();
                        }
                        else { // scribble recognition mode
                             cancel = recogniser.detectScribble(newPoint);
                             if (cancel) {
                                System.out.println("CANCELLED!");
                                return;
                             }
                        }
                                                
                        if (recogniser.outOfInactiveArea(newPoint, lastCenter)) {
                            graphics.drawLine(previous.x,previous.y, newPoint.x, newPoint.y);
                            if (recogniser.isRecognising()) {
                                boolean forceNewStroke = currentMenu.isShowing();
                                if (recogniser.detectStroke(newPoint,lastCenter, time, forceNewStroke)) {
                                    markingTimer.stop();
                                    if (showStrokes)
                                        graphics.setColor(Color.RED);
                                    graphics.drawLine(previous.x,previous.y, newPoint.x, newPoint.y);
                                     if (showStrokes)
                                        graphics.setColor(defaultColor);
                                    slice = recogniser.getSlice(newPoint,previous);
                                    if (!currentMenu.isShowing()) {
                                        graphics.setColor(mark);
                                        renderString(currentMenu.getItem(slice).getName(),previous.x,previous.y);
                                        graphics.setColor(defaultColor);
                                    }
                                    else {
                                         highlightItem(slice,currentMenu.getItem(slice).getName(),lastCenter.x,lastCenter.y);
                                    }
                                    Item newMenu = currentMenu.selectItem(slice);
                                    if (newMenu!=null) {//submenu selected
                                        currentMenu = (Menu)newMenu;
                                       
                                    }
                                    previousCenter = (Point)lastCenter.clone();
                                    lastCenter = (Point)newPoint.clone();
                                }
                             }
                             previous = (Point)newPoint.clone();
                             time = System.currentTimeMillis();
                        }
                        time = System.currentTimeMillis();
                    }
                   

    }
    
    private void highlightItem(int slice, String itemName, int x, int y) {
        graphics.setColor(mark);
        double amplitude = 2*Math.PI / currentMenu.getItemsCount();
        double currentRadian = amplitude/2;
         for (int i =0; i<currentMenu.getItemsCount(); i++) {
             if (i==slice) {
                showMenuItem(itemName, currentRadian + 0.5*amplitude,x,y);
                break;
             }
             currentRadian+= amplitude;
         }
        graphics.setColor(defaultColor);
        
    }
    
    private Point centerMenuItem(Point p, String name) {
        //-- Offset calculation in pixels, to center text. Do not compute offset on y (height)
        // get metrics from the graphics
        FontMetrics metrics = graphics.getFontMetrics(MARKING_FONT);
        // get the advance of my text in this font and render context
        int pixel_Width = metrics.stringWidth(name);
        int offset_Width;
        if (pixel_Width % 2 != 0) {
            offset_Width = ((pixel_Width - 1) / 2) + 1;
        } else {
            offset_Width = pixel_Width / 2;
        }
        p.x = p.x - offset_Width;
        return p;
    }
    
    private void drawMenuItemTab(int centerX, int centerY,double angle)
    {
        Point pointInnerCircle = polarToCartesian(centerX, centerY, angle, innerRadius);
        Point pointEndTab = polarToCartesian(centerX, centerY, angle, innerRadius+ itemTagSize);
        graphics.drawLine(pointInnerCircle.x,pointInnerCircle.y,pointEndTab.x,pointEndTab.y);
    }
    
    private void  showMenuCircle(int centerX,int centerY)
    {
        //int outerCircle = 0; //just for test, to implement if conclusive
        //graphics.drawOval(centerX - outerRadius-outerCircle, centerY - outerRadius-outerCircle, (outerRadius+outerCircle) * 2, (outerRadius+outerCircle) * 2);  //outside circle
        graphics.drawOval(centerX - innerRadius, centerY - innerRadius, innerRadius * 2, innerRadius * 2); //inner circle
        graphics.fillOval(centerX - innerRadius, centerY - innerRadius, innerRadius * 2, innerRadius * 2);
     }
    
    
}
