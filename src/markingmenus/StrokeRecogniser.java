package markingmenus;

import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author Mai
 */
public class StrokeRecogniser {
    
     private final int slices;
     private final float amplitude;
     private Stroke lastStroke;
     private int strokes;
     //private final float deadZoneFactor = 10;
     private int deadZone = 50;
     private final int innerRadius;
     private final int depth;
     private final ArrayList<Point> scribble;
     private final int scribblePoints = 50; 
     private final int scribbleRadius = 50; 
     private Point scribbleCenter;

    
     public StrokeRecogniser(int slices, int innerRadius, int depth) {
        this.slices = slices;
        this.innerRadius = innerRadius;
        this.depth = depth;
        amplitude= (float) (2*Math.PI)/slices;
        lastStroke = new Stroke();
        strokes = 0;
        scribble = new ArrayList<>();
     }
     
     
     private boolean inCircularArea(Point point, Point center, int radius) { 
        //determines if a point is in a circular area
        if (point!=null && center!=null) {
            float dx = point.x - center.x;
            float dy = point.y - center.y;
            return ((dx*dx + dy*dy) <= (radius*radius));
        }
        return false;
     }
     
     public boolean outOfInactiveArea(Point point, Point center) {
         return !inCircularArea(point,center,innerRadius);
     }
     
     
     public int getSlice(Point end, Point start) {
         // returns the selected slice; the numbering is counter-clockwise. Slice 0 is in the North
         int x = end.x - start.x;
         int y = end.y - start.y;
         double radius = Math.sqrt( x*x + y*y);
         double angle = Math.acos(x/radius); //normalised radius used here
         if (y > 0) {
              angle = 2*Math.PI - angle; // necessary because of properties of acos function
         }
        angle = 2*Math.PI + angle;
        double currentRadian = Math.PI / 4;
        
        int count = 0;
        for (int i = 0; i < 2*slices; i++) {
           if ((currentRadian < angle) && (angle <= currentRadian + amplitude)) {
             return count % slices;
           }
           count++;
           angle -= amplitude;
        }
        return -1;
       }
     
    public boolean isRecognising() {
        return (strokes<depth);
    }
     
  
    public boolean detectScribble(Point p) {
        if (scribbleCenter==null)
            scribbleCenter = p;
        if (inCircularArea(p,scribbleCenter,scribbleRadius)) {
            scribble.add(p);
            //update center with new point
            scribbleCenter.x = ( scribbleCenter.x * (scribble.size()-1)  + p.x ) /scribble.size();
            scribbleCenter.y = ( scribbleCenter.y * (scribble.size()-1)  + p.y ) /scribble.size();
            return scribble.size()==scribblePoints;
        }
        
        return false;
        
    }
    
     public boolean detectStroke(Point point, Point previous, long time, boolean forceNewStroke) {

            float dx = point.x - previous.x;
            float dy = point.y - previous.y;

        
            double distance = Math.sqrt( dx*dx + dy*dy);
//            if (time==0) { //sometimes the time measured between dragging events is 0 (not enough precision with long?)
//                time=1;
//            }
            
            //double speed = distance/time; //deadzone according to the speed of the mouse while dragging
            //double deadZone = speed/deadZoneFactor;  //faster movements are less precise --> more pronounced change required
            
          
            int movementX = Stroke.NO_MOVEMENT;
            if (Math.abs(dx) > deadZone) 
                if (dx > 0)
                    movementX =  Stroke.RIGHT;
                else 
                    movementX = Stroke.LEFT;

            int movementY = Stroke.NO_MOVEMENT;
            if (Math.abs(dy) > deadZone)
                if (dy > 0)
                    movementY =  Stroke.DOWN;
                else 
                    movementY = Stroke.UP;
            
            Stroke stroke;
            if (movementX!=Stroke.NO_MOVEMENT || movementY!=Stroke.NO_MOVEMENT) {
                stroke = new Stroke();
                stroke.setDirectionX(movementX);
                stroke.setDirectionY(movementY);
                if ( forceNewStroke || !lastStroke.equals(stroke)) {
                    lastStroke = stroke;
                    strokes++;
                    System.err.println("Stroke detected: "+lastStroke.toString());
                    return true;
                }
            }
            

            
        
        return false;      
        
     }
}
