
package markingmenus;

/**
 *
 * @author Mai
 */
public class Stroke {
    
    public static int RIGHT = 1;
    public static int LEFT = -1;
    public static int UP = -2;
    public static int DOWN = 2;
    public static int NO_MOVEMENT = 0;
    
    private int dirX = NO_MOVEMENT;
    private int dirY = NO_MOVEMENT;
    
    
    public Stroke() { 
        
    }
    
    public void setDirectionX(int dirX) {
        this.dirX = dirX;
        
    }
    
    public void setDirectionY(int dirY) {
        this.dirY = dirY;
        
    }

    public int getDirX() {
        return dirX;
    }

    public int getDirY() {
        return dirY;
    }
    
    
    
    public boolean equals(Object other) {
        Stroke otherStroke  = (Stroke)other;
        return (otherStroke.getDirX() == this.dirX && (otherStroke.getDirY()==this.dirY));
               

        
    }
    
    public String toString() {
        return "dirX: "+dirX+", dirY: "+dirY;
    }
    
}
