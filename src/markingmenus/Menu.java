package markingmenus;

import java.util.ArrayList;
import javax.swing.JLayer;

/**
 *
 * @author Mai
 */
public class Menu extends Item {
    
    public static int NONE = -1;
    private final ArrayList<Item> items;
    private int selectedItem = NONE;
    private boolean displayed;
    
    public Menu(String name) {
        super(name);
        items = new ArrayList<>();
        displayed = false;
    }
    
    public void addItem(Item item) {
        items.add(item);
        
    }
    
    public int getItemsCount() {
        return items.size();
    }
    
    public Item selectItem(int number) { 
        if (number<items.size()) {
            selectedItem = number;    //but we don't execute here! in case the user wants to cancel
            return items.get(number).select();
        }
        return null;
    }
    
    @Override
    public void restartSelection() {
        selectedItem = NONE;
        for (int i=0; i<items.size();i++) {
            Item item = items.get(i);
            item.restartSelection();
        }
    }
    
    public Item getSelectedItem() {
        if (selectedItem!=NONE && selectedItem<items.size())
            return items.get(selectedItem); 
        return null;
    }
    
    public Item getItem(int number) { 
        if (number<items.size())
            return items.get(number);
        return null;
    }
    
    public Item select() {
        System.out.println("Selected: "+this.getName()); 
        return this;
    }
    
    public void setDisplayed(boolean value) {
        this.displayed =value;
    }
    
    public boolean isShowing() { 
        return displayed;
    }
    
//    public Object execute(Object param) {
//        return null;
//        
//    }
    public void execute(JLayer l) {
        System.out.println("TRYING TO EXECUTE SUBMENU");
    }
    

    
}
