/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package markingmenus;

import javax.swing.JLayer;

/**
 *
 * @author Mai
 */
public abstract class Item{
    
    protected String name;
    
    public Item(String name) {
        this.name =name ;
    }
    public abstract Item select();
    //public abstract Object execute(Object param);
    public abstract void execute(JLayer l);

    public String getName() {
        return name;
    }
    
    public abstract void restartSelection();
 
    
}
